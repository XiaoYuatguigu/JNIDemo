//
// Created by hachiadmin on 2019/6/13.
//这个是linux 下jni的头文件，而java下也有jni.h头文件但是他是windows平台下的，有些地方不一样
#include <jni.h>
//////////////////////////
#include <android/log.h>
//Android 低层封装了c++的log，这个是属于宏定义
#define LOGE(...) __android_log_print(ANDROID_LOG_ERROR,"FFMPEG",__VA_ARGS__)
////使用c++里面封装的string是要加这两
#include <string>


using std::string;
///////////////////////////////////////

//本地方法的实现
void get_str(JNIEnv *env, jobject jobj) {
    LOGE("get_str");
    LOGE("get_str: %s", "运行了");
}

/**
 * 方法对应表
 */
static const JNINativeMethod mMethods[] = {
        {"call", "()V", (void *) get_str}
};


//需要动态注册native方法的类名
static const char *mClassName = "com/demo/jintest/JniManage";
////////////////////////////该方法在调用System.loadLibrary("native-lib");时候会执行/////////////////////////////////////
/*
* System.loadLibrary("lib")时调用
* 如果成功返回JNI版本, 失败返回-1
*/
jint JNI_OnLoad(JavaVM *vm, void *reserved) {
    JNIEnv *env = NULL;
    //获得 JniEnv
    int r = vm->GetEnv((void **) &env, JNI_VERSION_1_4);
    if (r != JNI_OK) {
        return -1;
    }
    jclass mainActivityCls = env->FindClass(mClassName);
    // 注册 如果小于0则注册失败
    r = env->RegisterNatives(mainActivityCls, mMethods, 1);
    if (r != JNI_OK) {
        return -1;
    }
    return JNI_VERSION_1_4;
}

/*
* 为所有类注册本地方法
*/




